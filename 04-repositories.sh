#!/bin/bash
#
# Mejorando Canaimita
# Authors:
# Jesus Lara <jesuslara@devel.com.ve>
# version: 0.1
# Copyright (C) 2012 Jesus Lara

# debian
DEBIAN="deb file:///media/jesuslara_bk/mirrors/debian/ wheezy main contrib non-free"

## backports
BACKPORTS="deb file:///media/jesuslara_bk/mirrors/backports/ squeeze-backports main contrib non-free"

echo $DEBIAN > /etc/apt/sources.list.d/debian.list
echo $BACKPORTS >> /etc/apt/sources.list.d/debian.list

MIRROR_LIQUORIX="deb file:///media/jesuslara_bk/mirrors/liquorix sid main"
# actualizacion del kernel
echo $MIRROR_LIQUORIX > /etc/apt/sources.list.d/liquorix.list

# evitar la validacion del repositorio
echo "Acquire::Check-Valid-Until "false";" > /etc/apt/apt.conf.d/80mintupdate-debian

# actualizacion de repositorios
$APT update

# instalacion de keyrings
$APT install --yes liquorix-archive-keyring liquorix-keyring liquorix-keyrings debian-archive-keyring debian-keyring debian-ports-archive-keyring
