#!/bin/bash
#
# Mejorando Canaimita
# Authors:
# Jesus Lara <jesuslara@devel.com.ve>
# version: 0.1
# Copyright (C) 2012 Jesus Lara

# mejoras al xorg

# copiar los archivos de configuracion a /usr/share/X11
cp /canaimita/xorg/* /usr/share/X11/xorg.conf.d/

# ejecutar
mkfontdir /var/lib/defoma/x-ttcidfont-conf.d/dirs/TrueType

# crear
$ECHO "options i915 modeset=1" > /etc/modprobe.d/i915-kms.conf
$ECHO "INTEL_BATCH=1" > /etc/environment

# actualizar
$APTITUDE -t wheezy install mesa-utils libgl1-mesa-dri libgl1-mesa-dri-experimental
