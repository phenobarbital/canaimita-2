#!/bin/bash
#
# Mejorando Canaimita
# Authors:
# Jesus Lara <jesuslara@devel.com.ve>
# version: 0.1
# Copyright (C) 2012 Jesus Lara

# convertir ext2,ext3 a ext4

# tune2fs -O extents,uninit_bg,dir_index $ROOT
tune2fs -O extents,uninit_bg,dir_index $BOOT

umount $BOOT
e2fsck -fDC0 $BOOT

# hacer una copia de seguridad del fstab
cp -p /etc/fstab /etc/fstab.orig

# cambiar el fstab
$SED -i -e "/\/boot/ s/ext3/ext4/;/\// s/ext3/ext4/;s/ext2/ext4/" /etc/fstab
# cambiar a defaults
$SED -i -e "s/defaults,noatime/${FS_FLAGS}/" /etc/fstab

$CAT << _MSG
  ---------------------------------------
  | Es necesario reiniciar el sistema   |
  | 					|
  | Al reiniciar, favor entrar en modo  |
  | Recuperar (recovery) y ejecutar     |
  | /canaimita/chequear_fs.sh           |
  | 					|
  | DE NO HACERLO EL SISTEMA QUEDARA    |
  | INUTILIZABLE!!!                     |
  ---------------------------------------
_MSG

reboot
