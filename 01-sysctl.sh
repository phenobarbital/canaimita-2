#!/bin/bash
#
# Mejorando Canaimita
# Authors:
# Jesus Lara <jesuslara@devel.com.ve>
# version: 0.1
# Copyright (C) 2012 Jesus Lara

# incorpora mejoras al sysctl

cat <<EOF >> /etc/sysctl.conf

# VM
vm.dirty_ratio = 20
vm.dirty_background_ratio = 20
vm.dirty_bytes = 67108864
vm.dirty_background_bytes = 134217728

# swapping
vm.swappiness = 10
vm.vfs_cache_pressure = 40
vm.min_free_kbytes=65536

# fs
fs.aio-max-nr = 1048576
fs.file-max = 287573

#cantidad de hilos maximos
kernel.threads-max = 98006

#networking
net.core.rmem_default = 8388608
net.core.rmem_max = 16777216
net.core.wmem_default = 8388608
net.core.wmem_max = 16777216
net.core.optmem_max = 40960
net.ipv4.tcp_rmem = 4096 87380 16777216
net.ipv4.tcp_wmem = 4096 65536 16777216
net.ipv4.tcp_mem = 8388608 8388608 8388608
net.ipv4.tcp_congestion_control = cubic
net.ipv4.tcp_rfc1337 = 1
net.ipv4.tcp_sack = 1
net.ipv4.tcp_fack = 1
net.ipv4.tcp_window_scaling = 1
net.ipv4.tcp_max_tw_buckets = 500000
# number of packets to keep in backlog before the kernel starts dropping them
net.ipv4.tcp_max_syn_backlog = 3240000
# increase socket listen backlog
net.core.somaxconn = 3240000
net.ipv4.tcp_max_tw_buckets = 1440000


# memory
kernel.sem = 100 32000 100 128

EOF

# y aplicamos los cambios
$SYSCTL -p
