#!/bin/bash
#
# Mejorando Canaimita
# Authors:
# Jesus Lara <jesuslara@devel.com.ve>
# version: 0.1
# Copyright (C) 2012 Jesus Lara

# version
KERNEL="3.2.0-0.bpo.3-rt-686-pae"
FIRMWARE="firmware-linux-free firmware-realtek"

# instalacion de nucleo nuevo
$APTITUDE -t wheezy install linux-image-$KERNEL linux-headers-$KERNEL $FIRMWARE

# actualizar plymouth
$APTITUDE -t wheezy install plymouth
$APTITUDE -t wheezy install libx11-dev

# desinstalar paquetes innecesarios
$APTITUDE purge libdrm-nouveau1a xserver-xorg-video-all xserver-xorg-video-6 libdrm-nouveau1 xserver-xorg-video-nouveau

# mas paquetes innecesarios
$APTITUDE purge `$APTITUDE search -F %p xserver-xorg-video-*`
# NOTA: A la primera solicitud, indicar que NO (presione N)
# a la segunda sugerencia (no se elimina ningún paquete de Canaima, pero se actualiza Xorg, presione S).

# actualizo a una nueva version el xorg y sus modulos principales
$APTITUDE -t wheezy install xserver-xorg-input-synaptics xserver-xorg-input-evdev xserver-xorg-video-intel

# desintalo los virtualbox (por alguna razon, estan alli)
$APTITUDE purge virtualbox-guest-x11 virtualbox-guest-utils
