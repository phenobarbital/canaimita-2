#!/bin/bash
#
# Mejorando Canaimita
# Authors:
# Jesus Lara <jesuslara@devel.com.ve>
# version: 0.1
# Copyright (C) 2012 Jesus Lara

CAT="$(which cat)"
GREP="$(which grep)"
AWK="$(which awk)"
SED="$(which sed)"
MOUNT="$(which mount)"
ECHO="$(which echo)"
SYSCTL="$(which sysctl)"
APT="$(which apt-get)"
APTITUDE="$(which aptitude)"

# Cambios iniciales
GRUB_FLAGS="acpi=ht elevator=cfq transparent_hugepages=madvise i915.powersave=1 i915.i915_enable_fbc=1 enable_mtrr_cleanup mtrr_spare_reg_nr=1 rootfstype=ext4"
FS_FLAGS="rw,noatime,data=writeback,i_version,barrier=0"

# elimino el archivo net-rules
rm -fr /etc/udev/rules.d/70-persistent-net.rules

# numero de particion
NUMBER=$1

if [ -z "$NUMBER" ]; then
	$ECHO -n "Numero de particion HOME nueva (ej: 4 para /dev/sda4) : "
        read _NUMBER_
	if [ -z "$_NUMBER_" ]; then
	    $ECHO "ERROR: - No se puede continuar sin un numero de particion HOME, alto"
	    exit 1
	fi
        NUMBER=$_NUMBER_
fi

# DEVICE
DEVICE=/dev/sda$NUMBER

# determinar si el dispositivo existe
DISK=$(fdisk -lu | grep ${DEVICE} | awk '{print $1}')
	if [ -z "$DISK" ]; then
	    $ECHO "ERROR: - El Disco $DEVICE indicado como particion HOME no existe"
            exit 1
	fi

# evitar que sea la particion RAIZ o el actual HOME
ROOT=$(cat /etc/mtab | grep "/ " | awk '{print $1}')
BOOT=$(cat /etc/mtab | grep "/boot " | awk '{print $1}')

if [ "$DEVICE" == "$ROOT" ] || [ "$DEVICE" == "$BOOT" ]; then
	echo "Error: Ha seleccionado la particion equivocada $DEVICE, alto"
	exit 1
fi

################################
# paso 1: optimizacion de SYSCTL
###############################

CHANGES=$(egrep "vm.vfs_cache_pressure" /etc/sysctl.conf | tail)
if [ -z "$CHANGES" ]; then
    . /canaimita/01-sysctl.sh
fi

#############################
# paso 2: cambios en el grub
#############################

$SED -i -e "s/GRUB_CMDLINE_LINUX=\"\"/GRUB_CMDLINE_LINUX=\"${GRUB_FLAGS}\"/" /etc/default/grub
$SED -i -e "s/GRUB_CMDLINE_LINUX=\"\"/GRUB_CMDLINE_LINUX=\"${GRUB_FLAGS}\"/" /etc/default/burg

# actualizo BURG
/usr/sbin/update-grub2

##############################
# paso 3: conversion ext2>ext4
##############################

CHANGES=$(cat /etc/fstab | grep "/boot" | grep ext4)
if [ -z "$CHANGES" ]; then
    . /canaimita/02-convert-ext4.sh
fi

###############################
# paso 4: formato de nuevo HOME
###############################

CHANGES=$(cat /etc/mtab | grep "/home" | grep ext4)
if [ -z "$CHANGES" ]; then
    . /canaimita/03-create-home.sh
fi

# incorporar las mejoras de disco
$SED -i -e "/^exit 0/ i\/canaimita/optimice-disk.sh" /etc/rc.local

###############################
# paso 5: repositorios
###############################

if [ ! -f "/etc/apt/sources.list.d/liquorix.list" ]; then
    . /canaimita/04-repositories.sh
fi

###############################
# paso 6: nucleo linux y xorg
###############################

if [ ! -f "/etc/apt/sources.list.d/liquorix.list" ]; then
    . /canaimita/05-update-kernel.sh
fi
. /canaimita/06-update-xorg.sh
