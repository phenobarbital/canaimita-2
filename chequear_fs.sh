#!/bin/bash

ROOT=$(cat /etc/mtab | grep "/ " | awk '{print $1}')
BOOT=$(cat /etc/mtab | grep "/boot " | awk '{print $1}')
HOME=$(cat /etc/mtab | grep "/home " | awk '{print $1}')

# desmontar raiz
umount /boot
e2fsck -f $BOOT

umount /
# chequear root
e2fsck -f $ROOT

umount /home
# chequear home
e2fsck -f $ROOT

