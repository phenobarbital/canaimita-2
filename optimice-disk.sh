#!/bin/bash
# script para optimizar el acceso a disco de sistemas en noop o deadline
# autor: jesus lara jesuslara@gmail.com

# optimice server disk settings
/sbin/blockdev --setra 16384 /dev/sda
# queue depth to "975" (SSD related)
/bin/echo "975" > /sys/block/sda/queue/nr_requests
# turn off rotational (SSD related)
/bin/echo "0" > /sys/block/sda/queue/rotational
# read ahead and sectors read by disk
/bin/echo "8192" > /sys/block/sda/queue/read_ahead_kb
/bin/echo "4096" > /sys/block/sda/queue/max_sectors_kb

# enable write-caching
/sbin/hdparm -W1 -A1 /dev/sda
