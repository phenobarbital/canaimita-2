#!/bin/bash
#
# Mejorando Canaimita
# Authors:
# Jesus Lara <jesuslara@devel.com.ve>
# version: 0.1
# Copyright (C) 2012 Jesus Lara

# instalacion de paquetes adicionales

# desintalo los virtualbox (por alguna razon, estan alli)
$APTITUDE -t squeeze install compiz-core compiz-fusion-plugins-extra compiz-fusion-plugins-main compiz-gnome compiz-plugins compizconfig-backend-gconf compizconfig-settings-manager fusion-icon

# celestia y stellarium
$APTITUDE -t squeeze install stellarium celestia-gnome

# herramientas de reparacion accidental de discos, borrado de archivos, etc
$APTITUDE -t squeeze-backports install extundelete foremost testdisk
