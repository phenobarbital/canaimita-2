#!/bin/bash
#
# Mejorando Canaimita
# Authors:
# Jesus Lara <jesuslara@devel.com.ve>
# version: 0.1
# Copyright (C) 2012 Jesus Lara

HOME_FLAGS="rw,noatime,data=writeback,nodelalloc,i_version,barrier=0,inode_readahead_blks=64"

# crear nuevo HOME

$ECHO "= Dando formato a $DEVICE =\r"

mkfs.ext4 -b 4096 -m 0 -O extents,dir_index $DEVICE

# data=writeback y chequeo y revision cada 50 montajes de manera automática
tune2fs -o journal_data_writeback -c 50 -i 50 /dev/sda4

# montarlo temporalmente
mount -t ext4 $DEVICE /mnt

# copiar nuevo HOME
rsync -av /home/* /mnt

# desmontar
umount $DEVICE

#chequear
e2fsck -f $DEVICE

# obtener el blkid
UUID=$(blkid | grep sda$NUMBER | grep -oP 'UUID=[0-9a-zA-Z"-]+' | cut -d '"' -f2)

# comentar la anterior linea de HOME
$SED -i -e "/\/home/ s/UUID=/#UUID=/" /etc/fstab

# y agregar el nuevo HOME
$ECHO "UUID=$UUID /home ext4  $HOME_FLAGS,errors=remount-ro 0 2" >> /etc/fstab

